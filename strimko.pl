
input_file(['4', '1 2 2 4', '2 1 4 2', '3 4 1 3', '4 3 3 1', '2 2 3', '2 3 2', '3 3 1']).

% ligacoes(['1 2 2 4', '2 1 4 2', '3 4 1 3', '4 3 3 1']).

% c(Linha, Coluna, Stream, Valor)
valores([c(1,1,1,_), c(1,2,2,_), c(1,3,2,_), c(1,4,4,_),
         c(2,1,2,_), c(2,2,1,3), c(2,3,4,_), c(2,4,2,_),
         c(3,1,3,_), c(3,2,4,2), c(3,3,1,3), c(3,4,2,_),
         c(4,1,4,_), c(4,2,3,_), c(4,3,3,_), c(4,4,3,_)]).

% [c(1,1,1,4), c(1,2,2,2), c(1,3,2,3), c(1,4,4,1), c(2,1,2,1), c(2,2,1,3), c(2,3,4,2), c(2,4,2,4), c(3,1,3,2), c(3,2,4,4), c(3,3,1,1), c(3,4,3,3), c(4,1,4,3), c(4,2,3,1), c(4,3,3,4), c(4,4,1,2)]
solucao([c(1,1,1,4), c(1,2,2,2), c(1,3,2,3), c(1,4,4,1),
           c(2,1,2,1), c(2,2,1,3), c(2,3,4,2), c(2,4,2,4),
           c(3,1,3,2), c(3,2,4,4), c(3,3,1,1), c(3,4,3,3),
           c(4,1,4,3), c(4,2,3,1), c(4,3,3,4), c(4,4,1,2)]).

tamanho(2).
amplitude(X):-
    tamanho(T),
    X is T+1.

diferenca([],_,[]).
diferenca([X|Xs],L2,L3):-
    membro(X,L2),
    diferenca(Xs,L2,L3).

diferenca([X|Xs],L2,[X|L3]):-
    \+ membro(X,L2),
    diferenca(Xs,L2,L3).


corrige_linha(_,[],_).
corrige_linha(c(X1,Y1,S1,V1),[c(X2,Y2,S2,V2)|Xs],[c(X1,Y1,S1,V1)|Cs]):-
    (X1 = X2,
    V1 \= V2,
    write('X: '),
    write(c(X2,Y2,S2,V2)),nl,
    write('Cs1: '),
    write([c(X2,Y2,S2,V2)|Cs]),nl,
    corrige_linha(c(X1,_,_,V1),Xs,Cs));
    (X1 = X2,
    V1 = V2,
    amplitude(A),
    random(1,A,N),
    /*write('N: '),
    write(N),nl,
    write('V1: '),
    write(V1),nl,*/
    corrige_linha(c(X1,_,_,N),[c(X2,_,_,V2)|Xs],Cs));
    (X1 \= X2,
    write('Cs2: '),
    write([c(X1,Y1,S1,V1)|Cs]),nl,
    corrige_linha(c(X1,_,_,V1),Xs,Cs)).

corrige_linhas([_|[]],[]).
corrige_linhas([X,Y|Xs],Rs):-
    %write('resto: '),write([Y|Xs]),nl,
    write('X antes: '),
    write(X),nl,
    corrige_linha(X,[Y|Xs],[R|Rs]),
    %write('Y: '),write(Y),nl,
    corrige_linhas([Y|Xs],[R|Rs]).


corrige_coluna(_,[]).
corrige_coluna(c(_,Y1,_,V1),[c(_,Y2,_,V2)|Xs]):-
    (Y1 = Y2,
    V1 \= V2,
    /*write(V1),
    write(' '),
    write(V2),
    write(' '),*/
    corrige_coluna(c(_,Y1,_,V1),Xs));
    (Y1 = Y2,
    V1 = V2,
    amplitude(A),
    random(1,A,N),
    /*write('N: '),
    write(N),nl,
    write('V1: '),
    write(V1),nl,*/
    corrige_coluna(c(_,Y1,_,N),[c(_,Y2,_,V2)|Xs]));
    (Y1 \= Y2,
    corrige_coluna(c(_,Y1,_,V1),Xs)).

corrige_colunas([_|[]]).
corrige_colunas([X,Y|Xs]):-
    %write('resto: '),write([Y|Xs]),nl,
    corrige_coluna(X,[Y|Xs]),
    %write('Y: '),write(Y),nl,
    corrige_colunas([Y|Xs]).


corrige_stream(_,[]).
corrige_stream(c(_,_,S1,V1),[c(_,_,S2,V2)|Xs]):-
    (S1 = S2,
    V1 \= V2,
    /*write(V1),
    write(' '),
    write(V2),
    write(' '),*/
    corrige_stream(c(_,_,S1,V1),Xs));
    (S1 = S2,
    V1 = V2,
    amplitude(A),
    random(1,A,N),
    /*write('N: '),
    write(N),nl,
    write('V1: '),
    write(V1),nl,*/
    corrige_stream(c(_,_,S1,N),[c(_,_,S2,V2)|Xs]));
    (S1 \= S2,
    corrige_stream(c(_,_,S1,V1),Xs)).

corrige_streams([_|[]]).
corrige_streams([X,Y|Xs]):-
    %write('resto: '),write([Y|Xs]),nl,
    corrige_stream(X,[Y|Xs]),
    %write('Y: '),write(Y),nl,
    corrige_streams([Y|Xs]).


verifica_linha(_,[]).
verifica_linha(c(X1,_,_,V1),[c(X2,_,_,V2)|Xs]):-
    number(V1),
    ((X1 = X2,
        V1 \= V2,
        linha(c(X1,_,_,V1),Xs));
        (X1 \= X2,
        linha(c(X1,_,_,V1),Xs))).

verifica_linhas([_|[]]).
verifica_linhas([X,Y|Xs]):-
    verifica_linha(X,[Y|Xs]),
    verifica_linhas([Y|Xs]).


verifica_coluna(_,[]).
verifica_coluna(c(_,Y1,_,V1),[c(_,Y2,_,V2)|Xs]):-
    number(V1),
    ((Y1 = Y2,
        V1 \= V2,
        coluna(c(_,Y1,_,V1),Xs));
        (Y1 \= Y2,
        verifica_coluna(c(_,Y1,_,V1),Xs))).

verifica_colunas([_|[]]).
verifica_colunas([X,Y|Xs]):-
    verifica_coluna(X,[Y|Xs]),
    verifica_colunas([Y|Xs]).


verifica_stream(_,[]).
verifica_stream(c(_,_,S1,V1),[c(_,_,S2,V2)|Xs]):-
    number(V1),
    ((S1 = S2,
        V1 \= V2,
        verifica_stream(c(_,_,S1,V1),Xs));
        (S1 \= S2,
        verifica_stream(c(_,_,S1,V1),Xs))).

verifica_streams([_|[]]).
verifica_streams([X,Y|Xs]):-
    verifica_stream(X,[Y|Xs]),
    verifica_streams([Y|Xs]).



strimko(valores(X)):-
/*
    (verifica_linhas();
    corrige_linhas(),
    verifica_colunas(),
    verifica_streams());
        (verifica_colunas();
        corrige_colunas(),
        verifica_linhas(),
        verifica_streams());
            (verifica_streams();
            corrige_streams(),
            verifica_colunas(),
            verifica_linhas()).
*/
    (linhas(X),
    colunas(X),
    streams(X));
    strimko(valores(X)).



/*
    O que falta fazer:
     - meter isto a papar do ficheiro
     - construir a matriz dinamicamente e popular com os valores dados nas coordenadas certas
     - contruir lista com os valores "corrigidos" para depois fazer as 3 verificações (linhas, colunas e stream)
*/
